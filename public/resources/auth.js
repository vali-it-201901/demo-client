function login() {
  const username = document.getElementById("username").value;
  const password = document.getElementById("password").value;
  return fetch("http://localhost:8080/login", {
    method: "POST",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({username: username, password: password})
  }).then(response => this.setTokenAndRedirectIfAuthorized(response));
}

function setTokenAndRedirectIfAuthorized(response) {
  if (response != null && response.headers.get("Authorization") != null) {
    localStorage.setItem("token", response.headers.get("Authorization"));
    document.location = "/";
  }
}

function clearToken() {
  localStorage.removeItem("token");
}

function getToken() {
  return localStorage.getItem("token");
}

function addAuthorizationHeader(headers) {
  headers["Authorization"] = this.getToken();
  return headers;
}

function isAuthenticated() {
  return this.getToken() != null;
}

function textToJson(text, defaultValue) {
  return text.length ? JSON.parse(text) : defaultValue;
}

function clearTokenAndRedirectIfUnauthorized(response) {
  if (response.status === 403) {
    this.clearToken();
    document.location = "/login.html";
  }
  return response;
}
