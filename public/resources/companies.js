function getCompanies() {
  fetch("http://localhost:8080/companies", {
    headers: addAuthorizationHeader({})
  })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => response.json())
    .then(companies => displayCompanies(companies));
}

function deleteCompany(id) {
  if (confirm("Oled sa kindel, et soovid antud ettevõtet kustutada?")) {
    const deleteUrl = "http://localhost:8080/company/" + id;
    fetch(deleteUrl, {
      method: "DELETE",
      headers: addAuthorizationHeader({})
    })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => getCompanies());
  }
}

function addOrEditCompany() {
  if (validateCompany()) {
    const id = document.getElementById("companyId").value;
    if (id > 0) {
      editCompany();
    } else {
      addCompany();
    }
  }
}

function validateCompany() {
  const name = document.getElementById("companyName").value;
  const logo = document.getElementById("companyLogo").value;

  if (name == null || name.length < 1) {
    displayCompanyValidationError("Ettevõtte nimi on puudu!");
    return false;
  }

  if (logo == null || logo.length < 1) {
    displayCompanyValidationError("Ettevõtte logo on puudu!");
    return false;
  }

  hideCompanyValidationError();
  return true;
}

function addCompany() {
  const name = document.getElementById("companyName").value;
  const logo = document.getElementById("companyLogo").value;

  const addUrl = "http://localhost:8080/company";
  fetch(addUrl, {
    method: "POST",
    headers: addAuthorizationHeader({"Content-Type": "application/json"}),
    body: JSON.stringify({
      name: name,
      logo: logo
    })
  })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getCompanies();
      closeCompanyModal();
    });
}

function editCompany() {
  const id = document.getElementById("companyId").value;
  const name = document.getElementById("companyName").value;
  const logo = document.getElementById("companyLogo").value;

  const editUrl = "http://localhost:8080/company";
  fetch(editUrl, {
    method: "PUT",
    headers: addAuthorizationHeader({"Content-Type": "application/json"}),
    body: JSON.stringify({
      id: id,
      name: name,
      logo: logo
    })
  })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getCompanies();
      closeCompanyModal();
    });
}

function displayCompanies(companies) {
  let companyList = document.getElementById("companyList");
  companyList.innerHTML = "";

  for (let i = 0; i < companies.length; i++) {
    companyList.innerHTML += `
      <tr>
        <td>${companies[i].id}</td>
        <td>${companies[i].name}</td>
        <td><img height="50" src="${companies[i].logo}" /></td>
        <td>
          <div class="companyRowButtons">
            <button
              type="button" class="btn btn-danger"
              onClick="deleteCompany(${companies[i].id})">Kustuta</button>
          </div>
          <div class="companyRowButtons">
            <button
              type="button" class="btn btn-dark"
              onClick="openCompanyModal(${companies[i].id})">Muuda</button>
          </div>
        </td>
      </tr>
    `;
  }
}

function openCompanyModal(id) {
  document.getElementById("companyId").value = null;
  document.getElementById("companyName").value = null;
  document.getElementById("companyLogo").value = null;
  $("#exampleModal").modal("show");

  if (id > 0) {
    const getCompanyUrl = "http://localhost:8080/company/" + id;
    fetch(getCompanyUrl, {headers: addAuthorizationHeader({})})
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => response.json())
      .then(company => {
        document.getElementById("companyId").value = company.id;
        document.getElementById("companyName").value = company.name;
        document.getElementById("companyLogo").value = company.logo;
      });
  }
}

function closeCompanyModal() {
  $("#exampleModal").modal("hide");
}

function displayCompanyValidationError(errorText) {
  let errorDiv = document.getElementById("companyValidationError");
  errorDiv.style.display = "block";
  errorDiv.innerHTML = errorText;
}

function hideCompanyValidationError() {
  let errorDiv = document.getElementById("companyValidationError");
  errorDiv.style.display = "none";
  errorDiv.innerHTML = "";
}
